package com.lizaveta;

/**
 * Created by Elizaveta on 24.07.2016.
 */
public class Main {
    public static void main(String[] args) {
        Person p1 = new Person(false, "Masha");
        Person p2 = new Person(true, "Pasha");
        Person p3 = new Person(true, "Sasha");

        System.out.println(p1.marry(p2));
        System.out.println(p2.marry(p1));
        System.out.println(p1.marry(p2));
        System.out.println(p1.divorce());
        System.out.println(p2.marry(p1));
        System.out.println(p2.divorce());
        System.out.println(p3.marry(p2));
    }
}
