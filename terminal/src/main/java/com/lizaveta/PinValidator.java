package com.lizaveta;

import com.lizaveta.Exceptions.AccountIsLockedException;
import com.lizaveta.Exceptions.PinValidatorException;

/**
 * Created by Elizaveta on 10.08.2016.
 */
public class PinValidator {
    private static final int INVALID_PIN_LIMIT = 3;

    public boolean pinIsValid(Account account, String pin) {
        if (account.getInvalidPinCount() == INVALID_PIN_LIMIT) {
            account.setInvalidPinCount(0);
            throw new AccountIsLockedException("Account is locked. Wait for 5 seconds.");
        } else if (pin.equals(account.getPin())) {
            account.setInvalidPinCount(0);
            return true;
        } else {
            account.setInvalidPinCount(account.getInvalidPinCount() + 1);
            StringBuilder warn = new StringBuilder().append("Invalid pin. ")
                    .append(INVALID_PIN_LIMIT - account.getInvalidPinCount())
                    .append(" attempts is left.");
            throw new PinValidatorException(warn.toString());
        }
    }
}
