package com.lizaveta;

/**
 * Created by Elizaveta on 10.08.2016.
 */
public class Account {
    private final String pin;
    private int balance;
    private int invalidPinCount;

    public Account(String pin, int balance) {
        this.pin = pin;
        this.balance = balance;
    }

    public String getPin() {
        return pin;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getInvalidPinCount() {
        return invalidPinCount;
    }

    public void setInvalidPinCount(int invalidPinCount) {
        this.invalidPinCount = invalidPinCount;
    }
}

