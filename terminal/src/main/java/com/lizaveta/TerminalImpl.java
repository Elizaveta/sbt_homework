package com.lizaveta;

import com.lizaveta.Exceptions.AccountIsLockedException;
import com.lizaveta.Exceptions.PinValidatorException;
import com.lizaveta.Exceptions.TerminalServerException;

/**
 * Created by Elizaveta on 10.08.2016.
 */
public class TerminalImpl implements Terminal {

    private final TerminalServer server;

    private final PinValidator pinValidator;

    public TerminalImpl(TerminalServer server, PinValidator pinValidator) {
        this.server = server;
        this.pinValidator = pinValidator;
    }

    public void checkBalance(Account account, String pin) {
        try {
            if (pinValidator.pinIsValid(account, pin)) {
                server.checkBalance(account, pin);
            }
        } catch (AccountIsLockedException | PinValidatorException e) {
            System.out.println(e.getMessage());
        }
    }

    public void getCash(Account account, String pin, int sum) {
        try {
            if (pinValidator.pinIsValid(account, pin)) {
                server.getCash(account, pin, sum);
            }
        } catch (AccountIsLockedException | PinValidatorException | TerminalServerException e) {
            System.out.println(e.getMessage());
        }
    }

    public void insertCash(Account account, String pin, int sum) {
        try {
            if (pinValidator.pinIsValid(account, pin)) {
                server.insertCash(account, pin, sum);
            }
        } catch (AccountIsLockedException | PinValidatorException | TerminalServerException e) {
            System.out.println(e.getMessage());
        }
    }
}
