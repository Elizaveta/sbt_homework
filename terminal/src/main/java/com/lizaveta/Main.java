package com.lizaveta;

/**
 * Created by Elizaveta on 10.08.2016.
 */
public class Main {

    public static void main(String[] args) {
        Account account = new Account("qwerty", 2000);
        TerminalServer server = new TerminalServer();
        PinValidator pinValidator = new PinValidator();
        Terminal terminal = new TerminalImpl(server, pinValidator);

        System.out.println(server.checkBalance(account, "qwerty"));
        terminal.checkBalance(account, "qweer");
        terminal.checkBalance(account, "dw");
        terminal.checkBalance(account, "aw");
        terminal.checkBalance(account, "qwerty");

        terminal.insertCash(account, "qwerty", 11);
        System.out.println(account.getBalance());
        terminal.getCash(account, "qwerty", 200);
        System.out.println(account.getBalance());

    }
}
