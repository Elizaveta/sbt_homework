package com.lizaveta;

import com.lizaveta.Exceptions.TerminalServerException;
import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

/**
 * Created by Elizaveta on 10.08.2016.
 */
public class TerminalServer {

    public int checkBalance(Account account, String pin) {
        return account.getBalance();
    }

    public void insertCash(Account account, String pin, int sum) {
        if (sum % 100 == 0) {
            account.setBalance(account.getBalance() + sum);
        } else {
            throw new TerminalServerException("Try another sum.");
        }
    }

    public void getCash(Account account, String pin, int sum) {
        if (sum <= account.getBalance()) {
            if (sum % 100 == 0) {
                account.setBalance(account.getBalance() - sum);
            } else {
                throw new TerminalServerException("Try another sum.");
            }
        } else {
            throw new TerminalServerException("Not enough money. Try another sum.");
        }
    }

}
