package com.lizaveta;

/**
 * Created by Elizaveta on 10.08.2016.
 */
public interface Terminal {

    public void checkBalance(Account account, String pin);

    public void getCash(Account account, String pin, int sum);

    public void insertCash(Account account, String pin, int sum);
}
