package com.lizaveta.Exceptions;

/**
 * Created by Elizaveta on 10.08.2016.
 */
public class TerminalServerException extends RuntimeException {
    public TerminalServerException(String cause) {
        super(cause);
    }
}
