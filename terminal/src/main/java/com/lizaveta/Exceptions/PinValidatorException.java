package com.lizaveta.Exceptions;

/**
 * Created by Elizaveta on 10.08.2016.
 */
public class PinValidatorException extends RuntimeException {
    public PinValidatorException(String cause) {
        super(cause);
    }
}
