package com.lizaveta.Exceptions;

/**
 * Created by Elizaveta on 10.08.2016.
 */
public class AccountIsLockedException extends RuntimeException {
    public AccountIsLockedException(String cause) {
        super(cause);
    }
}
