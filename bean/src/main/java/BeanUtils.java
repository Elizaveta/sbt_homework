import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class BeanUtils {

    public static void assign(Object to, Object from) throws NoSuchMethodException {
        Class<?> classTo = to.getClass();
        Class<?> classFrom = from.getClass();
        List<Method> settersTo = new ArrayList<>();
        Map<Method, Class> methodFromMap = new HashMap<>();

        for (Method methodFrom : classFrom.getMethods()) {
            if (methodFrom.getName().startsWith("get") && methodFrom.getParameterCount() == 0) {
                methodFromMap.put(methodFrom, methodFrom.getReturnType());
            }
        }

        for (Method methodTo : classTo.getMethods()) {
            if (methodTo.getName().startsWith("set") && methodTo.getParameterCount() > 0) {
                settersTo.add(methodTo);
            }
        }

        try {
            for (Method mth : settersTo) {
           /* if (mth.getName().substring(mth.getName().lastIndexOf("set") + 3).equals(entry.getKey().getName().substring(entry.getKey().getName().lastIndexOf("get") + 3))) {
                System.out.println(mth.getName());
            }*/
                String nameTo = mth.getName().substring(mth.getName().lastIndexOf("set") + 3);
                Method m = classFrom.getMethod("get" + nameTo);
                if (methodFromMap.containsKey(m)) {
                    mth.invoke(to, m.invoke(from));
                }
            }
        } catch (NoSuchMethodException | IllegalArgumentException | InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException("reflection exception, smth wrong with access");
        }
    }
}
