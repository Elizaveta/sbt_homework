import java.util.*;

public class CountMapImpl<T> implements CountMap<T> {

    private final Map<T, Integer> map = new HashMap<>();

    @Override
    public void add(T k) {
        if (!map.containsKey(k)) {
            map.put(k, 1);
        } else {
            map.put(k, map.get(k) + 1);
        }
    }

    @Override
    public int getCount(T k) {
        return (map.containsKey(k)) ? map.get(k) : 0;
    }

    @Override
    public int remove(T k) {
        return (map.containsKey(k)) ? map.remove(k) : 0;
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public void addAll(CountMap<T> source) {
        for (T k : source.toMap().keySet())
            if (map.containsKey(k)) {
                map.put(k, map.get(k) + source.getCount(k));
            } else {
                map.put(k, source.getCount(k));
            }
    }

    @Override
    public Map<T, Integer> toMap() {
        return map;
    }

    @Override
    public void toMap(Map<T, Integer> destination) {
        destination = map;
    }

}
