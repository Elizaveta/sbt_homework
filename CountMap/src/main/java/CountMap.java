import java.util.Map;

public interface CountMap<T> {

    void add(T k);

    int getCount(T k);

    int remove(T k);

    int size();

    void addAll(CountMap<T> source);

    Map<T, Integer> toMap();

    void toMap(Map<T, Integer> destination);
}
