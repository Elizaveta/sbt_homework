import javafx.util.converter.NumberStringConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        List<String> l1 = new ArrayList<>();
        List<Integer> l2 = new ArrayList<>();
        List<Number> l3 = new ArrayList<>();

        CountMap<String> mp = new CountMapImpl<>();
        CountMap<String> mp2 = new CountMapImpl<>();

        mp.add("A");
        mp.add("K");
        mp.add("A");
        //     System.out.println(mp.size());

        mp2.add("A");
        mp2.add("K");
        mp2.add("L");

        mp.addAll(mp2);

        System.out.println(mp.size());
        System.out.println(mp.getCount("A"));
    }
}
