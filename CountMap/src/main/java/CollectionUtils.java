import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class CollectionUtils<T> {

    private final List<T> list = null;

    public static <T> void addAll(List<? extends T> source, List<? super T> destination) {
        destination.addAll(source);
    }

    public static <T> List<T> newArrayList() {
        return new ArrayList<T>();
    }

    public static <T> int indexOf(List<? extends T> source, T o) {
        return source.indexOf(o);
    }

    public static <T> List limit(List source, int size) {
        return null;
    }

    public static <T> void add(List<? super T> source, T o) {
        source.add(o);
    }

    public static <T> void removeAll(List<? extends T> removeFrom, List<? super T> c2) {
        removeFrom.removeAll(c2);
    }

    public static <T> boolean containsAll(List<T> c1, List<T> c2) {
        return c1.containsAll(c2);
    }

    public static <T> boolean containsAny(List<T> c1, List<T> c2) {
        for (T entry : c1) {
            if (c2.contains(entry)) return true;
        }
        return false;
    }

    public static <T extends Comparable> List<T> range(List<? extends T> list, T min, T max) {
        for (T e : list) {
            if (e.compareTo())
        }
        return null;
    }

    public static <T> List range(List<? extends T> list, T min, T max, Comparator<? super T> comparator) {
        list.sort(comparator);
        return list.subList(list.indexOf(min), list.indexOf(max) + 1);
    }

}
